import "./main.css";
import "./car-car.css";
import "./background.css";
import movingCircles from "./main-page-circles";
import Nav from "./Nav";

function MainPage() {
  return (

    <>
    <Nav />
    <div className="inv-box">
      <a href="/automobiles/" class="link">
        <svg
          viewBox="0 0 200 200"
          width="200"
          height="200"
          xmlns="http://www.w3.org/2000/svg"
          class="link__svg"
          aria-labelledby="link1-title link1-desc"
        >
          <path
            id="link-circle"
            class="link__path"
            d="M 20, 100 a 80,80 0 1,1 160,0 a 80,80 0 1,1 -160,0"
            stroke="none"
            fill="none"
          />
          <path
            class="link__arrow"
            d="M 75 100 L 125 100 L 110 85 M 125 100 L 110 115"
            fill="none"
          />
          <text class="link__text">
            <textPath href="#link-circle" stroke="none">
              check cars in inventory
            </textPath>
          </text>
        </svg>
      </a>
      <a href="/appointments/new" class="link">
        <svg
          viewBox="0 0 200 200"
          width="200"
          height="200"
          xmlns="http://www.w3.org/2000/svg"
          class="link__svg"
          aria-labelledby="link1-title link1-desc"
        >
          <path
            id="link-circle"
            class="link__path"
            d="M 20, 100 a 80,80 0 1,1 160,0 a 80,80 0 1,1 -160,0"
            stroke="none"
            fill="none"
          />
          <path
            class="link__arrow"
            d="M 75 100 L 125 100 L 110 85 M 125 100 L 110 115"
            fill="none"
          />
          <text class="link__text-2">
            <textPath href="#link-circle" stroke="none">
              schedule an appointment
            </textPath>
          </text>
        </svg>
      </a>
      </div>


      <div className="company-name-box">
        another div another doller
      </div>
      </>
  );
}

export default MainPage;
