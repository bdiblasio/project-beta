import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';


function AppointmentList () {
    const [appointments, setAppointments] = useState([])
    const fetchData = async () => {
        const url = "http://localhost:8080/api/appointments/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setAppointments(data.appointments)
        }
    }


    const deleteData = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}`, {method: 'DELETE'})
      }


    const completeAppointment = async (id) => {
        const completeUrl = `http://localhost:8080/api/appointments/${id}/`
        const status = {"status": "COMPLETE"}
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(status),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(completeUrl, fetchConfig)
        fetchData()
    }


    const navigate = useNavigate()
    const directToForm = () => {
        navigate("new/")
    }

    
    useEffect(() => {
        fetchData()
    }, [])


    return <>
        <br />
        <h1>Service Appointment List</h1>
        <br />
        <div id="appointments_list">
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>vin</th>
                    <th>customer name</th>
                    <th>VIP status</th>
                    <th>date</th>
                    <th>time</th>
                    <th>technician</th>
                    <th>reason</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {appointments?.filter((appointment) => appointment.status !== "COMPLETE")?.map(appointment => {
                    return (
                        <tr >
                            <td>{ appointment.vin }</td>
                            <td>{ appointment.customer_name }</td>
                            <td>{ appointment.vip_status }</td>
                            <td>{ appointment.date }</td>
                            <td>{ appointment.time }</td>
                            <td>{ appointment.technician.name }</td>
                            <td>{ appointment.reason }</td>
                            <td><button onClick={() => deleteData(appointment.id)} className="btn btn-outline-danger">cancel</button></td>
                            <td><button onClick={() => completeAppointment(appointment.id)} value={appointment.status} className="btn btn-success">complete</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </div>
        <button
          onClick={directToForm}
          style={{ backgroundColor: "blue", color: "white" }}
        >
          Add a new appointment
        </button>
        </>
}

export default AppointmentList
