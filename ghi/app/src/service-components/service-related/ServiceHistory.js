import { useState, useEffect } from 'react';


function ServiceHistory () {

    const [appointments, setAppointments] = useState([])
    const [filterValue, setfilterValue] = useState('')

    const fetchData = async () => {
        const url = "http://localhost:8080/api/appointments/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setAppointments(data.appointments)
        }
    }


    const handleChange = (e) => {
        setfilterValue(e.target.value);
      };


    useEffect(() => {
        fetchData()
    }, [])

    
    return <>
    <br />
    <h1>Service Appointment History</h1>
    <br />
    <input onChange={handleChange} placeholder="Filter For Vin" />

    <div id="appointments_list">
    <table className="table table-striped">
        <thead>
            <tr>
                <th>vin</th>
                <th>customer name</th>
                <th>date</th>
                <th>time</th>
                <th>technician</th>
                <th>reason</th>
            </tr>
        </thead>
        <tbody>
            {appointments?.filter((appointment) =>
                    appointment.vin.includes(filterValue)
                    ).map(appointment => {
                    return (
                    <tr >
                        <td>{ appointment.vin }</td>
                        <td>{ appointment.customer_name }</td>
                        <td>{ appointment.date }</td>
                        <td>{ appointment.time }</td>
                        <td>{ appointment.technician.name }</td>
                        <td>{ appointment.reason }</td>
                    </tr>
                )
            })}
        </tbody>
    </table>
    </div>
    </>
}

export default ServiceHistory
