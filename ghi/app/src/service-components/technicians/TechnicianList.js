import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';


function TechnicanList () {
    const [technicians, setTechnicians] = useState([])
    const fetchData = async () => {
        const TechnicianUrl = 'http://localhost:8090/api/employees/'
        const response = await fetch(TechnicianUrl)
        if (response.ok) {
            const data = await response.json()
            console.log(data)
            setTechnicians(data.employees)
        }
    }


    const deleteData = async (id) => {
        const response = await fetch(`http://localhost:8090/api/employees/${id}/`, {method: 'DELETE'})
    }




    useEffect(() => {
        fetchData()
    }, [])


    return <>
    <br />
    <h1>Technician List</h1>
    <br />
    <div id="technician-list">
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>ID</th>
                </tr>
            </thead>
            <tbody>
                {technicians?.map(technician => {
                    return (
                        <tr>
                            <td>{ technician.employee_name }</td>
                            <td>{ technician.id }</td>
                            <td><button onClick={() => deleteData(technician.id)} style={{ backgroundColor: "red", color: "white" }} className="btn btn-outline-danger">Delete</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </div>
    </>
}

export default TechnicanList
