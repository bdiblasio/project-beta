import React, { useState, useEffect } from "react";

function AddToInventory() {
  const [models, setModels] = useState([]);
  const [formData, setFormData] = useState({
    color: "",
    year: "",
    vin: "",
    model_id: "",
  });

  const getData = async () => {
    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const inventoryUrl = "http://localhost:8100/api/automobiles/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(inventoryUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        color: "",
        year: "",
        vin: "",
        model_id: "",
      });
    } else console.log("problem");
  };

  const handleChangeInput = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };
  return (
    // <div className="row">
    //   <div className="offset-3 col-6">
    //     <div id="employee_create">
          <div>
            <div>
              <div>

          <h3>Add a New Car to the Inventory</h3>
          <form onSubmit={handleSubmit} id="create_employee_form">
          <div>
              <label htmlFor="name">
                Color
              </label>
              <input
                value={formData.color}
                onChange={handleChangeInput}
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
            </div>
            <div className="mb-2">
              <label htmlFor="year" className="form-label">
                Year
              </label>
              <input
                value={formData.year}
                onChange={handleChangeInput}
                type="text"
                name="year"
                id="year"
                className="form-control"
              />
            </div>
            <div className="mb-2">
              <label htmlFor="vin" className="form-label">
                Enter the VIN
              </label>
              <input
                value={formData.vin}
                onChange={handleChangeInput}
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
            </div>

            <div className="mb-2">
              <label htmlFor="vin" className="form-label">
                Model ID
              </label>
              <input
                value={formData.model_id}
                onChange={handleChangeInput}
                type="number"
                name="model_id"
                id="model_id"
                className="form-control"
              />
            </div>

            <button className="btn btn-primary">Add Model to Inventory</button>
          </form>

        </div>
      </div>
    </div>
  );
}

export default AddToInventory
