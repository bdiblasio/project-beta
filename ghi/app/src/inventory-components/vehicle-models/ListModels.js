import { useEffect, useState } from "react";

function ListModels() {
  const [models, setModels] = useState([]);
  const getModels = async () => {
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  useEffect(() => {
    getModels();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Model ID</th>
          <th>Manufacturer</th>
          <th>Model</th>
          <th>Pic Url</th>
        </tr>
      </thead>
      <tbody>
        {models?.map((model) => {
          return (
            <tr key={model.href}>
              <td>{model.id}</td>
              <td>{model.manufacturer.name}</td>
              <td>{model.name}</td>
              <td>
                <img src={model.picture_url} alt="Car Model" />
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ListModels;
