import {useState, useEffect} from "react"

function NewSalesForm() {
    const [employees, setEmployees] = useState([])
    const [customers, setCustomers] = useState([])
    const [autos, setAutomobiles] = useState([])
    const [formData, setFormData] = useState({
        sales_price: '',
        sales_customer_id: '',
        sales_employee_id: '',
        automobile_id: '',
    })

    const getData = async () => {
        const employeesUrl = 'http://localhost:8090/api/employees/'
        const customersUrl = 'http://localhost:8090/api/customers/'
        const autoUrl= 'http://localhost:8100/api/automobiles/'

        const employeeResponse = await fetch(employeesUrl)
        const customerResponse = await fetch (customersUrl)
        const autoResponse = await fetch (autoUrl)

        if (employeeResponse.ok) {
            const employeeData = await employeeResponse.json()
            setEmployees(employeeData.employees)

        if (customerResponse.ok) {
            const customerData = await customerResponse.json()
            setCustomers(customerData.customers)
        }

        if (autoResponse.ok) {
            const autoData = await autoResponse.json()
            setAutomobiles(autoData.autos)
        }
    }
}

    useEffect (() => {
        getData()
    }, [])

    const handleSubmit = async (e) => {
        e.preventDefault()

        const salesUrl = 'http://localhost:8090/api/sales/'

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {'Content-Type':'application/json'}
        }

        const salesResponse = await fetch(salesUrl, fetchConfig)
        console.log(salesResponse)
        if (salesResponse.ok) {
            setFormData({
                sales_price: '',
                sales_customer_id: '',
                sales_employee_id: '',
                autos: '',
            })
        }
    }
    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name
        setFormData({
            ...formData,
            [inputName] : value
        })
    }
    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Record a Sale</h1>
              <form onSubmit={handleSubmit} id="create-sales-form">
              <div className="form-floating mb-3">
                  <input
                    onChange={handleFormChange}
                    value={formData.sales_price}
                    placeholder="Sales Price"
                    required
                    type="number"
                    name="sales_price"
                    id="sales_price"
                    className="form-control"
                  />
                  <label htmlFor="sales_price">Sales Price</label>
                </div>
                <div className="form-floating mb-3">
                    <select value={formData.autos} onChange={handleFormChange} required name="autos" id="autos" className="form-select">
                        <option value="">Choose an Automobile</option>
                        {autos.map(auto => {
                            return (
                                <option key={auto.id} value={auto.id}>
                                    {auto.model.manufacturer.name} {auto.model.name} || {auto.color}, {auto.year}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <select value={formData.sales_customer_id} onChange={handleFormChange} required name="sales_customer_id" id="sales_customer_id" className="form-select">
                        <option value="">Choose a Customer</option>
                        {customers.map(customer => {
                            return (
                                <option key={customer.id} value={customer.id}>
                                    {customer.customer_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <select value={formData.sales_employee_id} onChange={handleFormChange} required name="sales_employee_id" id="sales_employee_id" className="form-select">
                        <option value="">Choose an Employee</option>
                        {employees.map(employee => {
                            return (
                                <option key={employee.id} value={employee.id}>
                                    {employee.employee_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
    )
}


export default NewSalesForm;
