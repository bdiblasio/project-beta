//has been edited to remove Bootstrap

import React, {useState, useEffect} from 'react';


function NewEmployeeForm () {
    const [employees, setEmployees] = useState([])
    const [formData, setFormData] = useState({
        employee_name: '',
    })


    const handleSubmit = async (event) => {
        event.preventDefault()
        const employeeUrl = 'http://localhost:8090/api/employees/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
              'Content-Type': 'application/json',
            },
          };

        const response = await fetch(employeeUrl, fetchConfig)

        if (response.ok) {
            setFormData({
                employee_name: '',
            });
          }
    }

    const handleChangeInput = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        })
    }
    return (
        <div>
            <div>
            <div>

                <h3>Add Employee</h3>
                    <form onSubmit={handleSubmit} id="create_employee_form">
                        <div className='mb-2'>
                            <div>
                                <label htmlFor="employee_name">Employee Name</label>
                                <input value={formData.employee_name} onChange={handleChangeInput} type="text" name="employee_name" id="employee_name"/>
                            </div>
                        </div>
                        <button>Create a New Employee</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default NewEmployeeForm
