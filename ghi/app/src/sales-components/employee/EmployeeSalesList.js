import { useState, useEffect } from "react";

function EmployeeSalesList() {
  const [salesData, setSalesData] = useState([]);
  const [employees, setEmployees] = useState([]);

  const getEmployees = async () => {
    const employeesUrl = "http://localhost:8090/api/employees";
    const response = await fetch(employeesUrl);

    if (response.ok) {
      const data = await response.json();
      setEmployees(data.employees);
    } else {
      alert("Something went wrong with the employees API");
    }
  };

  const getSales = async () => {
    const salesUrl = "http://localhost:8090/api/sales/";
    const response = await fetch(salesUrl);
    if (response.ok) {
      const data = await response.json();
      setSalesData(data.sales);
      return data.sales;
    } else {
      alert("Something went wrong with the sales API");
    }
  };

  const handleChange = async (event) => {
    const sales = await getSales();
    setSalesData(sales);
    const filteredList = sales.filter(
      (sale) => sale.sales_employee.id == event.target.value
    );
    setSalesData(filteredList);
  };

  useEffect(() => {
    getEmployees();
    getSales();
  }, []);

  return (
    <div>
      <br />
      <h1>Sales History</h1>
      <div>
        <select onChange={handleChange} required id="employee" name="employee">
          <option value="">Choose Salesperson to See Record</option>
          {employees?.map((employee) => {
            return (
              <option key={employee.id} value={employee.id}>
                {employee.employee_name}
              </option>
            );
          })}
        </select>
        <h2>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>SALESPERSON</th>
                <th>CUSTOMER</th>
                <th>VIN</th>
                <th>SALE PRICE</th>
              </tr>
            </thead>
            <tbody>
              {salesData?.map((saleData) => {
                return (
                  <tr key={saleData.id}>
                    <td>{saleData.sales_employee.employee_name}</td>
                    <td>{saleData.sales_customer.customer_name}</td>
                    <td>{saleData.automobile.vin}</td>
                    <td>{saleData.sales_price}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </h2>
      </div>
    </div>
  );
}

export default EmployeeSalesList;
