import { useEffect, useState } from "react";

function ListAllCustomers() {
  const [customers, setCustomers] = useState([]);
  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/customers/");
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  };
  useEffect(() => {
    getData();
  }, []);
  return (
    <table className="table table-striped">
      <thead>
        <tr>
            <th>Customer</th>
            <th>Address</th>
            <th>Phone </th>
            <th>Reference ID </th>
        </tr>
      </thead>
      <tbody>
        {customers?.map((customer) => {
          return (
            <tr key={customer.id}>
              <td>{customer.customer_name}</td>
              <td>{customer.customer_address}</td>
              <td>{customer.customer_phone}</td>
              <td>{customer.id}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ListAllCustomers;
