import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AppointmentList from './service-components/appointments/AppointmentList';
import TechnicianList from './service-components/technicians/TechnicianList';
import AppointmentForm from './service-components/appointments/AppointmentForm';
import ManufacturerList from './inventory-components/manufacturers/ManufacturerList';
import ManufacturerForm from './inventory-components/manufacturers/ManufacturerForm';
import ListModels from './inventory-components/vehicle-models/ListModels';
import ListAllSales from './sales-components/sales-record/ListAllSales';
import NewEmployeeForm from './sales-components/employee/NewEmployeeForm';
import NewCustomerForm from './sales-components/customer/NewCustomerForm';
import EmployeeSalesList from './sales-components/employee/EmployeeSalesList';
import NewSalesForm from './sales-components/sales-record/NewSalesForm';
import NewModelForm from './inventory-components/vehicle-models/NewModelForm';
import ListAllCustomers from './sales-components/customer/ListCustomers';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
{/* //APPOINTMENTS */}
        <Route path="/add-appointment" element={<AppointmentForm />} />
        <Route path="/appointments" element={<AppointmentList />} />

{/* //EMPLOYEES */}
        <Route path="/employees" element={<TechnicianList />} />
        <Route path="/add-employee" element={<NewEmployeeForm />} />

{/* //MANUFACTURERS */}
        <Route path="/manufacturers" element={<ManufacturerList />} />
        <Route path="/add-manufacturer" element={<ManufacturerForm />} />

{/* //AUTOMOBILES */}
        <Route path="/automobiles" element={<AutomobileList />} />
        <Route path="/add-automobile" element={<AutomobileForm />} />

{/* //SALES */}
        <Route path="/sales" element={<ListAllSales />} />
        <Route path ="/employee-sales" element={<EmployeeSalesList/>} />
        <Route path ="/new-sale" element={<NewSalesForm/>} />

{/* //MODELS */}
        <Route path="/models" element={<ListModels />} />
        <Route path ="add-model" element={<NewModelForm/>} />

{/* //CUSTOMERS */}
        <Route path ="/add-customer" element={<NewCustomerForm/>} />
        <Route path ="/customers" element={<ListAllCustomers/>} />


      </Routes>
    </BrowserRouter>
  );
}

export default App;
