import { NavLink, Link } from "react-router-dom";
import "./navbar.css";

function Nav() {
  return (
    <>
      <input
        id="page-nav-toggle"
        className="main-navigation-toggle"
        type="checkbox"
      />
      <label htmlFor="page-nav-toggle">
        <svg className="icon--menu-toggle" viewBox="0 0 60 30">
          <g className="icon-group">
            <g className="icon--menu">
              <path d="M 6 0 L 54 0" />
              <path d="M 6 15 L 54 15" />
              <path d="M 6 30 L 54 30" />
            </g>
            <g className="icon--close">
              <path d="M 15 0 L 45 30" />
              <path d="M 15 30 L 45 0" />
            </g>
          </g>
        </svg>
      </label>

      <nav className="main-navigation">
        <ul>
          <h1><Link to="/"> Home </Link></h1>
          <h2> Automobiles </h2>
          <Link to="/automobiles">All Automobiles</Link>
          <Link to="/add-automobile/">Add Automobile</Link>
          <Link to="/add-inventory/">Add to Inventory</Link>

          <h2> Customers </h2>
          <Link to="/customers">All Customers</Link>
          <Link to="/add-customer">Add a Customer</Link>

          <h2> Appointments </h2>
          <Link to="/appointments">All Appointments</Link>
          <Link to="/add-appointment">Add Appointment</Link>

          <h2> Employees </h2>
          <Link to="/employees">All Employees</Link>
          <Link to="/add-employee"> Add an Employee</Link>

          <h2> Sales </h2>
          <Link to="/sales">All Sales</Link>
          <Link to="/new-sale">Add a New Sale</Link>
          <Link to="/employee-sales">Sales by Employee</Link>

          <h2> Models </h2>
          <Link to="/models"> All Models</Link>
          <Link to="/add-model">Add a Model</Link>

          <h2> Manufacturers </h2>
          <Link to="/manufacturers">All Manufacturers</Link>
          <Link to="/add-manufacturer">Add Manufacturer</Link>
        </ul>
      </nav>
    </>
  );
}

export default Nav;
