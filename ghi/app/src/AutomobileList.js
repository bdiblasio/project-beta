import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';


function AutomobileList () {
    const [automobiles, setAutomobiles] = useState([])
    const fetchData = async () => {
        const AutomobileUrl = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(AutomobileUrl)
        if (response.ok) {
            const data = await response.json()
            setAutomobiles(data.autos)
        }
    }


    const deleteData = async (id) => {
        const response = await fetch(`http://localhost:8100/api/automobiles/${id}/`, {method: 'DELETE'})
    }


    const navigate = useNavigate()
    const directToForm = () => {
        navigate("new/")
    }


    useEffect(() => {
        fetchData()
    }, [])

    
    return <>
    <br />
    <h1>Automobiles List</h1>
    <br />
    <div id="automobiles_list">
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Vin</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {automobiles?.map(automobile => {
                    return (
                        <tr>
                            <td>{ automobile.vin }</td>
                            <td>{ automobile.color }</td>
                            <td>{ automobile.year }</td>
                            <td>{ automobile.model.name }</td>
                            <td>{ automobile.model.manufacturer.name }</td>
                            <td> <img src={ automobile.model.picture_url } width="200px" ></img></td>
                            <td><button onClick={() => deleteData(automobile.vin)} style={{ backgroundColor: "red", color: "white" }} className="btn btn-outline-danger">Delete</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </div>
    <button onClick={directToForm} style={{ backgroundColor: "blue", color: "white" }}> Add a new automobile</button>
    </>
}

export default AutomobileList
