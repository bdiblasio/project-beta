# CarCar

Team:

* Person 1 - Ally - Services
* Person 2 - Bernadette - Sales

## Design
SALES DESIGN
    SALES FORMS:
    [add to nav] Sales Person Form (See Model)
    [add to nav] Customer Form (See Model)
    [add to nav] Sales Record Form (Automobile + Price + Employee + Customer)

    INTEGRATION:
    Inventory Microservice

SERVICE DESIGN
    The service has a form where a worker would add appointment details such as vin, customer name, date, time and reason. There is a dropdown on this form so the technican can be selected. There is a technician form where a new technician may be added. There are technician, service history, and appointment lists. On each of the lists there is an option to delete the entry. For the service history list, there is a filter method where the user is able to search by vin number. For the appointment list, the user is able to mark appointment as completed or cancel. 

## Service microservice

I have an AutomobileVO which uses a poller to get information about the automobiles and their vins that are currently in inventory and that is a foreign key for my appointment model. If there is an automobile vin that is in the inventory record then that person has a VIP status for their appointment. My other model is technician and is also a foreign key of appointment.


## Sales microservice
SALES MICROSERVICE
    MODELS:
    AutomobileVO -----> Service Poller -----> Inventory Microservice ----> Automobile Model from Inventory
    Sales Record (Model Name: SalesRecord)
        - FK: Employee ( See Below )
        - FK: Customer ( See Below )
        - AutomobileVO ( See Above )
    Sales Person (Model Name: Employee)
        - Name
        - ID
    Potential Customer (Model Name: Customer)
        - Name
        - Address
        - Phone Number

    POLLER:
        Between AutomobileVO and Inventory Microservice, Automobile Model

    VIEWS:
        - List all sales [add to Nav]
            - Employee
            - Employee Number
            - Customer
            - Automobile VIN
            - Price

        - List employee sales [add to Nav]
            - Employee
            - Customer of Specific Line-Item Sale
            - VIN of Specific Line-Item Sale
            - Price of Specific Line-Item Sale
